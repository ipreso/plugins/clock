// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugClock.h
 * Description: Manage Clock for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Improved plugin management
 *  - 2009.03.12: Original revision
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME "Clock"

int     initPlugin      (sPlugins * plugins, void * handle);

char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);

int     play            (sConfig * config, int wid);
int     prepare         (sConfig * config, int wid);
int     clean           ();

int addManagedMIME      (sPlugins * plugins,
                         void * handle,
                         char * proto,
                         char * type);
int isWindowAlive (sConfig * config, int wid);
